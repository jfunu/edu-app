

### Setup

    - run  "git clone https://jfunu@bitbucket.org/jfunu/edu-app.git"
    - cd edu-app/
    - run "composer install"
    - run "php artisan key:genenerate"
    - run "php artisan key:genenerate --env=testing"
    - run "chmod a+w storage/ bootstrap/cache/"
    - run "php artisan passport:install"
 
#### Database 
    
     - copy .env.example and name it .env 
     - create and database and update .env file with your database credentials
     - run "php artisan migrate"
 
 
 ### Start server
 
    - Run "php artisan serve" to start the server
 
 ### Generating a token for api requests
    
    - on home page click register to register a user
    - click "create new token" to generate a new token to a new token
    - Add headers 
        Accept: application/json
        Authorization: Bearer "token u generated"
  
  
 #### Endpoints
 ##### Courses
    - create a course
        - POST /api/courses
             - name: Name of the course
  
    - get all courses
        - GET /api/courses
    
    - get a single course
        - GET /api/courses/{courseId}
     
    - update a course
        - PUT /api/courses/{courseId}
     
    - delete a course
        - DELETE /api/courses/{courseId}
     


#### Subjects
    - create a subject
        - POST /api/subjects
            - name: Name of the subject
  
    - get all subjects
        - GET /api/subjects
    
    - get a single subject
        - GET /api/subjects/{subjectId}
     
    - update a subject
        - PUT /api/subjects/{subjectId}
     
    - delete a subject
        - DELETE /api/subjects/{subjectId}
     
     
#### Tutorials
     - create a tutorial
        - POST /api/tutorials
            - content: Content of the tutorial
  
    - get all tutorials
        - GET /api/tutorials
    
    - get a single tutorial
        - GET /api/tutorials/{tutorialId}
     
    - update a tutorial
        - PUT /api/tutorials/{tutorialId}
     
    - delete a tutorial
        - DELETE /api/tutorials/{tutorialId}
     
     
     
#### Quizzes
    - create a quiz
        - POST /api/quizes
            - content: Name of the quiz
  
    - get all quizes
        - GET /api/quizes
    
    - get a single quiz
        - GET /api/quizes/{quizId}
     
    - update a quiz
        - PUT /api/quizes/{quizId}
     
    - delete a quiz
        - DELETE /api/quizes/{quizId}
     
     
     
#### Questions
    - create a question
        - POST /api/questions
            - question: Question
            - answer : Answer to question ( eg. A or B or C)
            - possible_answers: possible answers to question
            - ```[
                    [
                        'option' => 'A',
                        'answer' => 'Option A'
                    ],
                    [
                         'option' => 'B',
                         'answer' => 'Option B'
                    ]
                 ]
  
    - get all questions
        - GET /api/questions
    
    - get a single question
        - GET /api/questions/{questionId}
     
    - update a question
        - PUT /api/questions/{questionId}
     
    - delete a question
        - DELETE /api/questions/{questionId}
     
     
### Course subjects
    - assign subjects to a course
        - POST /api/courses/{courseId}/subjectss
            - subjects : Ids of subjects [ 1, 3] 
    
    - get subjects of a course
        -GET /api/courses/{courseId}/subject
        
        
### Quiz questions
    - assign questions to a quiz
        - POST /api/quizes/{quizId}/questions
            - questions : Ids of questions [ 1, 3] 
    
    - get questions of a quiz
        -GET /api/quizes/{quizId}/questions
        
        
### Subject quizes
       - assign quizes to a subject
           - POST /api/subjects/{subjectId}/quizes
               - quizes : Ids of quizes [ 1, 3] 
       
       - get all quizes of a subject
           -GET /api/subjects/{subjectId}/quizes 

     
     
### Subject tutorial
       - assign tutorials to a subject
           - POST /api/subjects/{subjectId}/tutorials
               - tutorials : Ids of tutorials [ 1, 3] 
       
       - get all tutorials of a subject
           -GET /api/subjects/{subjectId}/tutorials 
   