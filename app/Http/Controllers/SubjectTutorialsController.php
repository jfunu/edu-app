<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectTutorialsController extends Controller
{

    public function index(Subject $subject)
    {
        $tutorials = $subject->tutorials;

        return response()->json([
            'data' => [
                'tutorials' => $tutorials
            ]
        ] ,200);
    }

    public function store(Subject $subject)
    {
        $subject->tutorials()->attach(request('tutorials'));

        return response()->json([], 201);
    }
}
