<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectQuizesController extends Controller
{

    public function index(Subject $subject)
    {
        $quizes = $subject->quizes;

        return response()->json([
            'data' => [
                'quizes' => $quizes
            ]
        ] ,200);
    }

    public function store(Subject $subject)
    {
        $subject->quizes()->attach(request('quizes'));

        return response()->json([], 201);
    }
}
