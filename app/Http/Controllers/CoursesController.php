<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseRepository;
use Exception;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    private $courseRepo;

    public function __construct(CourseRepository $courseRepo){
        $this->courseRepo = $courseRepo;
    }

    public function index()
    {
        // $courses = Course::all();
        $courses = $this->courseRepo->getAll();

        return response()->json([
            'data' => [
                'courses' => $courses
            ]
        ], 200);
    }

    public function show($id)
    {
        $course = $this->courseRepo->getById($id);

        return response()->json([
            'data' => [
                'course' => $course
            ]
        ], 200);
    }

    public function store()
    {
        $inputs = request()->only('name');
        $course = $this->courseRepo->create($inputs);

        return response()->json([
            'message' => 'Course created successfully',
            'data' => $course
        ], 201);
    }

    public function update($id)
    {
        $inputs = request()->only('name');

        $course = $this->courseRepo->update($id, $inputs);

        return response()->json([
            'message' => 'Course updated successfully',
            'data' => $course
        ], 200);
    }

    public function delete($id)
    {
        $this->courseRepo->delete($id);
        return response()->json([], 200);
    }
}
