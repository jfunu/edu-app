<?php

namespace App\Http\Controllers;

use App\Tutorial;
use Exception;
use Illuminate\Http\Request;

class TutorialsController extends Controller
{

    public function index()
    {
        $tutorials = Tutorial::all();

        return response()->json([
            'data' => [
                'tutorials' => $tutorials
            ]
        ], 200);
    }

    public function show(Tutorial $tutorial)
    {
        return response()->json([
            'data' => [
                'tutorial' => $tutorial
            ]
        ], 200);
    }

    public function store()
    {
        $tutorial = Tutorial::create(request()->only(['content']));

        return response()->json([
            'data' => [
                'tutorial' => $tutorial
            ]
        ], 201);
    }

    public function update(Tutorial $tutorial)
    {
        $tutorial->fill(request()->only(['content']))->save();

        return response()->json([
            'data' => [
                'tutorial' => $tutorial
            ]
        ], 200);
    }

    public function delete(Tutorial $tutorial)
    {
        try {
            $tutorial->delete();
        } catch (Exception $e) {
            return response()->json([], 422);
        }

        return response()->json([], 200);
    }
}
