<?php

namespace App\Http\Controllers;

use App\Quiz;
use Illuminate\Http\Request;

class QuizQuestionsController extends Controller
{

    public function index(Quiz $quiz)
    {
        $questions = $quiz->questions;

        return response()->json([
            'data' => [
                'questions' => $questions
            ]
        ] ,200);
    }


    public function store(Quiz $quiz)
    {
        $quiz->questions()->attach(request('questions'));

        return response()->json([], 201);
    }
}
