<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseSubjectsController extends Controller
{
    public function index(Course $course)
    {
        $subjects = $course->subjects;

        return response()->json([
            'data' => [
                'subjects' => $subjects
            ]
        ] ,200);
    }

    public function store(Course $course)
    {
        $course->subjects()->attach(request('subjects'));

        return response()->json([], 201);
    }
}
