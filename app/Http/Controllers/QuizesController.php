<?php

namespace App\Http\Controllers;

use App\Quiz;
use Exception;
use Illuminate\Http\Request;

class QuizesController extends Controller
{
    public function index()
    {
        $quizes = Quiz::all();

        return response()->json([
            'data' => [
                'quizes' => $quizes
            ]
        ], 200);
    }

    public function show(Quiz $quiz)
    {
        return response()->json([
            'data' => [
                'quiz' => $quiz
            ]
        ], 200);
    }


    public function store()
    {
        $quiz = Quiz::create([
            'name' => request('name')
        ]);

        return response()->json([
            'data' => [
                'quiz' => $quiz
            ]
        ], 201);
    }

    public function update(Quiz $quiz)
    {
        $quiz->fill(request()->only(['name']))->save();

        return response()->json([
            'data' => [
                'quiz' => $quiz
            ]
        ], 200);
    }

    public function delete(Quiz $quiz)
    {
        try {
            $quiz->delete();
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }

        return response()->json([], 200);
    }
}
