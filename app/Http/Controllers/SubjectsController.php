<?php

namespace App\Http\Controllers;

use App\Subject;
use Exception;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{

    public function index()
    {
        $subjects = Subject::all();

        return response()->json([
            'data' => [
                'subjects' => $subjects
            ]
        ], 200);
    }

    public function show(Subject $subject)
    {
        return response()->json([
            'data' => [
                'subject' => $subject
            ]
        ], 200);
    }

    public function store()
    {
        $subject = Subject::create(request()->only(['name']));

        return response()->json([
            'data' => [
                'subject' => $subject
            ]
        ], 201);
    }

    public function update(Subject $subject)
    {
        $subject->fill(request()->only(['name']))->save();

        return response()->json([
            'data' => [
                'subject' => $subject
            ]
        ], 200);
    }

    public function delete(Subject $subject)
    {
        try {
            $subject->delete();
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 422);
        }

        return response()->json([], 200);
    }
}
