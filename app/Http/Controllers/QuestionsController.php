<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{

    public function index()
    {
        $questions = Question::with(['possibleAnswers'])->get();

        return response()->json([
            'data' => [
                'questions' => $questions
            ]
        ], 200);
    }

    public function show(Question $question)
    {
        return response()->json([
            'data' => [
                'question' => $question
            ]
        ], 200);
    }


    public function store()
    {
        $question = Question::create([
            'question' => request('question'),
            'answer' => request('answer')
        ]);

        $question->saveAnswers(request('possible_answers'));

        return response()->json([], 201);
    }
}
