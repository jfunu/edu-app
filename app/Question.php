<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function possibleAnswers()
    {
        return $this->hasMany(Answer::class);
    }

    public function quizes()
    {
        return $this->belongsToMany(Quiz::class, 'quiz_questions');
    }

    public function saveAnswers($possibleAnswers)
    {
        $this->possibleAnswers()->createMany($possibleAnswers);
    }
}
