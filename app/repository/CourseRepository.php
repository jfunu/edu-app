<?php
namespace App;

use App\Repository\RepositoryInterface;

class CourseRepository implements RepositoryInterface {


    public function getAll()
    {
        return Course::all();
    }

    public function getById($id){
        return Course::findOrFail($id);
    }

    public function create($attributes){
        return Course::create($attributes);
    }

    public function update($id, $attributes){
        $course = $this->getById($id);
        $course->fill($attributes)->save();
        return $course;
    }

    public function delete($id){
        $course = $this->getById($id);
        $course->delete();
        return true;
    }

}