<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];

    public function quizes()
    {
        return $this->belongsToMany(Quiz::class, 'subject_quizes');
    }

    public function tutorials()
    {
        return $this->belongsToMany(Tutorial::class, 'subject_tutorials');
    }
}
