<?php

namespace Tests\Unit;

use App\Question;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    function a_question_can_save_its_possible_answers()
    {
        $question = factory(Question::class)->create();

        $question->saveAnswers([
            [
                'option' => 'A',
                'answer' => $this->faker->sentence
            ],
            [
                'option' => 'B',
                'answer' => $this->faker->sentence
            ],
            [
                'option' => 'C',
                'answer' => $this->faker->sentence
            ],
            [
                'option' => 'D',
                'answer' => $this->faker->sentence
            ],
        ]);

        $this->assertEquals(4, $question->fresh()->possibleAnswers->count());
    }
}
