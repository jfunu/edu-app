<?php

namespace Tests\Feature;

use App\Quiz;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateQuizTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_quiz_can_be_updated_successfully()
    {
        $this->withoutExceptionHandling();

        $quiz = factory(Quiz::class)->create([
            'name' => 'Mid-term'
        ]);

        $response = $this->put("/api/quizes/{$quiz->id}", [
            'name' => 'End of term'
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'quiz' => [
                    'name' => 'End of term'
                ]
            ]
        ]);

        $this->assertEquals('End of term', $quiz->fresh()->name);
    }

    /** @test */
    function updating_a_quiz_that_does_not_exist_should_return_a_404_response()
    {
        $response = $this->put("/api/quizes/1", [
            'name' => 'End of term'
        ]);

        $response->assertStatus(404);
    }

}
