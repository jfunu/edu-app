<?php

namespace Tests\Feature;

use App\Quiz;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateQuizTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_quiz_can_be_successfully_created()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/quizes', [
            'name' => 'Test one'
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, Quiz::all());
    }
}
