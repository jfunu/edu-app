<?php

namespace Tests\Feature;

use App\Quiz;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetQuizesTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_list_of_quizes_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $quizA = factory(Quiz::class)->create();
        $quizB = factory(Quiz::class)->create();

        $response = $this->get('/api/quizes');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'quizes' => [
                    ['name' => $quizA->name ],
                    ['name' => $quizB->name ],
                ]
            ]
        ]);
    }

    /** @test */
    function a_single_quiz_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $quiz = factory(Quiz::class)->create();

        $response = $this->get("/api/quizes/$quiz->id");

        $response->assertStatus(200);
        $this->assertEquals($quiz->name, $response->decodeResponseJson()['data']['quiz']['name']);
    }

}
