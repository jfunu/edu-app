<?php

namespace Tests\Feature;

use App\Quiz;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteQuizTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_quiz_can_be_deleted_successfully()
    {
        $this->withoutExceptionHandling();

        $quiz = factory(Quiz::class)->create();

        $this->assertCount(1, Quiz::all());

        $response = $this->delete("/api/quizes/{$quiz->id}");
        $response->assertStatus(200);

        $this->assertCount(0, Quiz::all());
    }


    /** @test */
    function deleting_a_quiz_that_does_not_exist_should_return_404_response()
    {
        $response = $this->delete("/api/quizes/1");
        $response->assertStatus(404);
    }
}
