<?php

namespace Tests\Feature;

use App\Course;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateCourseTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_course_can_be_successfully_created()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/courses', [
            'name' => 'General Arts'
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, Course::all());
    }
}
