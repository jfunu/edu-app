<?php

namespace Tests\Feature;

use App\Course;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetCoursesTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_list_of_courses_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $courseA = factory(Course::class)->create();
        $courseB = factory(Course::class)->create();

        $response = $this->get('/api/courses');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'courses' => [
                    ['name' => $courseA->name ],
                    ['name' => $courseB->name ],
                ]
            ]
        ]);
    }


    /** @test */
    function a_single_course_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $course = factory(Course::class)->create();

        $response = $this->get("/api/courses/$course->id");

        $response->assertStatus(200);
        $this->assertEquals($course->name, $response->decodeResponseJson()['data']['course']['name']);
    }
}
