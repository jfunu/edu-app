<?php

namespace Tests\Feature;

use App\Course;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateCourseTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_course_can_be_updated_successfully()
    {
        $this->withoutExceptionHandling();

        $course = factory(Course::class)->create();

        $response = $this->put("/api/courses/{$course->id}", [
            'name' => 'Visual Arts'
        ]);

        $response->assertStatus(200);
        $this->assertEquals('Visual Arts', $course->fresh()->name);
    }
}
