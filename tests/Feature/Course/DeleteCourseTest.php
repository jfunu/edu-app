<?php

namespace Tests\Feature;

use App\Course;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteCourseTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_course_can_be_deleted_successfully()
    {
        $this->withoutExceptionHandling();

        $course = factory(Course::class)->create();

        $this->assertCount(1, Course::all());

        $response = $this->delete("/api/courses/{$course->id}");
        $response->assertStatus(200);

        $this->assertCount(0, Course::all());
    }


    /** @test */
    function deleting_a_course_that_does_not_exist_should_return_404_response()
    {
        $response = $this->delete("/api/courses/1");
        $response->assertStatus(404);
    }
}
