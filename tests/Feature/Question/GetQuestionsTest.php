<?php

namespace Tests\Feature;

use App\Question;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetQuestionsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_list_of_questions_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $questionA = factory(Question::class)->create();
        $questionB = factory(Question::class)->create();

        $response = $this->get('/api/questions');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'questions' => [
                    ['question' => $questionA->question ],
                    ['question' => $questionB->question ],
                ]
            ]
        ]);
    }

    /** @test */
    function a_single_question_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $question = factory(Question::class)->create();

        $response = $this->get("/api/questions/$question->id");

        $response->assertStatus(200);
        $this->assertEquals($question->question, $response->decodeResponseJson()['data']['question']['question']);
    }

}
