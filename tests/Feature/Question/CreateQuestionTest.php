<?php

namespace Tests\Feature;

use App\Question;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateQuestionTest extends PassportTestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    function a_question_can_be_created_successfully_with_its_possible_answers()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/questions', [
            'question' => $this->faker->sentence,
            'answer' => 'A',
            'possible_answers' => [
                [
                    'option' => 'A',
                    'answer' => $this->faker->sentence
                ],
                [
                    'option' => 'B',
                    'answer' => $this->faker->sentence
                ],
                [
                    'option' => 'C',
                    'answer' => $this->faker->sentence
                ],
                [
                    'option' => 'D',
                    'answer' => $this->faker->sentence
                ],
            ]
        ]);

        $response->assertStatus(201);

        $this->assertCount(1, Question::all());
        $question = Question::first();
        $this->assertEquals(4, $question->possibleAnswers->count());
    }
}
