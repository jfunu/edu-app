<?php

namespace Tests\Feature;

use App\Subject;
use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetSubjectTutorialsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function tutorials_of_a_subject_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create();
        $tutorials = factory(Tutorial::class, 3)->create();
        $subject->tutorials()->attach($tutorials->pluck('id')->toArray());

        $response = $this->get("/api/subjects/$subject->id/tutorials");

        $response->assertStatus(200);
        $this->assertCount(3, $response->decodeResponseJson()['data']['tutorials']);
    }
}
