<?php

namespace Tests\Feature;

use App\Subject;
use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateSubjectTutorialsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function quizes_can_be_assigned_to_a_subject()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create();
        $tutorial1 = factory(Tutorial::class)->create();
        $tutorial2 = factory(Tutorial::class)->create();
        $tutorial3 = factory(Tutorial::class)->create();

        $response = $this->post("/api/subjects/$subject->id/tutorials", [
            'tutorials' => [$tutorial1->id, $tutorial3->id]
        ]);

        $response->assertStatus(201);
        $this->assertEquals(3, Tutorial::count());
        $this->assertEquals(2, $subject->tutorials->count());
    }
}
