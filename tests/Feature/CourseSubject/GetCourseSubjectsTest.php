<?php

namespace Tests\Feature;

use App\Course;
use App\Subject;
use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetCourseSubjectsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function subjects_of_a_course_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $course = factory(Course::class)->create();
        $subjects = factory(Subject::class, 3)->create();
        $course->subjects()->attach($subjects->pluck('id')->toArray());

        $response = $this->get("/api/courses/$course->id/subjects");

        $response->assertStatus(200);
        $this->assertCount(3, $response->decodeResponseJson()['data']['subjects']);
    }
}
