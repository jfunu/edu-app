<?php

namespace Tests\Feature;

use App\Course;
use App\Subject;
use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateCourseSubjectsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function subjects_can_be_assigned_to_a_course()
    {
        $this->withoutExceptionHandling();

        $course = factory(course::class)->create();
        $subject1 = factory(Subject::class)->create();
        $subject2 = factory(Subject::class)->create();
        $subject3 = factory(Subject::class)->create();

        $response = $this->post("/api/courses/$course->id/subjects", [
            'subjects' => [$subject1->id, $subject3->id]
        ]);

        $response->assertStatus(201);
        $this->assertEquals(3, Subject::count());
        $this->assertEquals(2, $course->subjects->count());
    }
}
