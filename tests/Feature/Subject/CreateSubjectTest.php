<?php

namespace Tests\Feature;

use App\Subject;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateSubjectTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_subject_can_be_created_successfully()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/subjects', [
            'name' => 'Mathematics'
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'subject' => [
                    'name' => 'Mathematics'
                ]
            ]
        ]);
        $this->assertCount(1, Subject::all());
    }
}
