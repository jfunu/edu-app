<?php

namespace Tests\Feature;

use App\Subject;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateSubjectTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_subject_can_be_updated_successfully()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create([
            'name' => 'Science'
        ]);

        $response = $this->put("/api/subjects/{$subject->id}", [
            'name' => 'English'
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'subject' => [
                    'name' => 'English'
                ]
            ]
        ]);

        $this->assertEquals('English', $subject->fresh()->name);
    }

    /** @test */
    function updating_a_subject_that_does_not_exist_should_return_a_404_response()
    {
        $response = $this->put("/api/subjects/1", [
            'name' => 'English'
        ]);

        $response->assertStatus(404);
    }
}
