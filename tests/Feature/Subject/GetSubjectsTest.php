<?php

namespace Tests\Feature;

use App\Subject;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetSubjectsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_list_of_subjects_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $subjectA = factory(Subject::class)->create();
        $subjectB = factory(Subject::class)->create();

        $response = $this->get('/api/subjects');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'subjects' => [
                    ['name' => $subjectA->name ],
                    ['name' => $subjectB->name ],
                ]
            ]
        ]);
    }

    /** @test */
    function a_single_subject_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create();

        $response = $this->get("/api/subjects/$subject->id");

        $response->assertStatus(200);
        $this->assertEquals($subject->name, $response->decodeResponseJson()['data']['subject']['name']);
    }

}
