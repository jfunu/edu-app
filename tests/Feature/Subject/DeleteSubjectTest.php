<?php

namespace Tests\Feature;

use App\Subject;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteSubjectTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_subject_can_be_deleted_successfully()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create();

        $this->assertCount(1, Subject::all());

        $response = $this->delete("/api/subjects/{$subject->id}");
        $response->assertStatus(200);

        $this->assertCount(0, Subject::all());
    }


    /** @test */
    function deleting_a_subject_that_does_not_exist_should_return_404_response()
    {
        $response = $this->delete("/api/subjects/1");
        $response->assertStatus(404);
    }
}
