<?php

namespace Tests\Feature;

use App\Question;
use App\Quiz;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetQuizQuestionsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function questions_of_a_quiz_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $quiz = factory(Quiz::class)->create();
        $questions = factory(Question::class, 5)->create();
        $quiz->questions()->attach($questions->pluck('id')->toArray());

        $response = $this->get("/api/quizes/$quiz->id/questions");

        $response->assertStatus(200);
        $this->assertCount(5, $response->decodeResponseJson()['data']['questions']);
    }
}
