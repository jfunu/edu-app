<?php

namespace Tests\Feature;

use App\Question;
use App\Quiz;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateQuizQuestionTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function questions_can_be_assigned_to_a_quiz()
    {
        $this->withoutExceptionHandling();

        $quiz = factory(Quiz::class)->create();
        $question1 = factory(Question::class)->create();
        $question2 = factory(Question::class)->create();
        $question3 = factory(Question::class)->create();

        $response = $this->post("/api/quizes/$quiz->id/questions", [
            'questions' => [$question1->id, $question2->id]
        ]);

        $response->assertStatus(201);
        $this->assertEquals(2, $quiz->questions->count());
    }
}
