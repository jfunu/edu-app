<?php

namespace Tests\Feature;

use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTutorialTest extends PassportTestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    function a_tutorial_can_be_created_successfully()
    {
        $this->withoutExceptionHandling();

        $content = $this->faker->paragraph;
        $response = $this->post('/api/tutorials', [
            'content' => $content
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'tutorial' => [
                    'content' => $content
                ]
            ]
        ]);
        $this->assertCount(1, Tutorial::all());
    }
}
