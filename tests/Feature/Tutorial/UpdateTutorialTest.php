<?php

namespace Tests\Feature;

use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTutorialTest extends PassportTestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    function a_tutorial_can_be_updated_successfully()
    {
        $this->withoutExceptionHandling();

        $tutorial = factory(Tutorial::class)->create([]);
        $content = $this->faker->paragraph;

        $response = $this->put("/api/tutorials/{$tutorial->id}", [
            'content' => $content
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'tutorial' => [
                    'content' => $content
                ]
            ]
        ]);

        $this->assertEquals($content, $tutorial->fresh()->content);
    }

    /** @test */
    function updating_a_tutorial_that_does_not_exist_should_return_a_404_response()
    {
        $response = $this->put("/api/tutorials/1", [
            'content' => $this->faker->paragraph
        ]);

        $response->assertStatus(404);
    }

}
