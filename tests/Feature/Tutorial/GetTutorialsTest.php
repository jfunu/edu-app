<?php

namespace Tests\Feature;

use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetTutorialsTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_list_of_tutorials_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $tutorialA = factory(Tutorial::class)->create();
        $tutorialB = factory(Tutorial::class)->create();

        $response = $this->get('/api/tutorials');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'tutorials' => [
                    ['content' => $tutorialA->content ],
                    ['content' => $tutorialB->content ],
                ]
            ]
        ]);
    }

    /** @test */
    function a_single_tutorial_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $tutorial = factory(Tutorial::class)->create();

        $response = $this->get("/api/tutorials/$tutorial->id");

        $response->assertStatus(200);
        $this->assertEquals($tutorial->content, $response->decodeResponseJson()['data']['tutorial']['content']);
    }

}
