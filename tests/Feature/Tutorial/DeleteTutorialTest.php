<?php

namespace Tests\Feature;

use App\Tutorial;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteTutorialTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function a_tutorial_can_be_deleted_successfully()
    {
        $this->withoutExceptionHandling();

        $tutorial = factory(Tutorial::class)->create();

        $this->assertCount(1, Tutorial::all());

        $response = $this->delete("/api/tutorials/{$tutorial->id}");
        $response->assertStatus(200);

        $this->assertCount(0, Tutorial::all());
    }


    /** @test */
    function deleting_a_tutorial_that_does_not_exist_should_return_404_response()
    {
        $response = $this->delete("/api/tutorials/1");
        $response->assertStatus(404);
    }
}
