<?php

namespace Tests\Feature;

use App\Quiz;
use App\Subject;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateSubjectQuizesTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function quizes_can_be_assigned_to_a_subject()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create();
        $quiz1 = factory(Quiz::class)->create();
        $quiz2 = factory(Quiz::class)->create();
        $quiz3 = factory(Quiz::class)->create();

        $response = $this->post("/api/subjects/$subject->id/quizes", [
            'quizes' => [$quiz1->id, $quiz3->id]
        ]);

        $response->assertStatus(201);
        $this->assertEquals(3, Quiz::count());
        $this->assertEquals(2, $subject->quizes->count());
    }
}
