<?php

namespace Tests\Feature;

use App\Quiz;
use App\Subject;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetSubjectQuizTest extends PassportTestCase
{
    use RefreshDatabase;

    /** @test */
    function quizes_of_a_subject_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $subject = factory(Subject::class)->create();
        $quizes = factory(Quiz::class, 3)->create();
        $subject->quizes()->attach($quizes->pluck('id')->toArray());

        $response = $this->get("/api/subjects/$subject->id/quizes");

        $response->assertStatus(200);
        $this->assertCount(3, $response->decodeResponseJson()['data']['quizes']);
    }
}
