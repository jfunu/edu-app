<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function(){
    Route::post('courses', 'CoursesController@store');
    Route::post('courses/{course}/subjects', 'CourseSubjectsController@store');
    Route::get('courses/{course}/subjects', 'CourseSubjectsController@index');
    Route::get('courses', 'CoursesController@index');
    Route::get('courses/{course}', 'CoursesController@show');
    Route::put('courses/{course}', 'CoursesController@update');
    Route::delete('courses/{course}', 'CoursesController@delete');

    Route::post('subjects', 'SubjectsController@store');
    Route::post('subjects/{subject}/quizes', 'SubjectQuizesController@store');
    Route::get('subjects/{subject}/quizes', 'SubjectQuizesController@index');
    Route::post('subjects/{subject}/tutorials', 'SubjectTutorialsController@store');
    Route::get('subjects/{subject}/tutorials', 'SubjectTutorialsController@index');
    Route::get('subjects', 'SubjectsController@index');
    Route::get('subjects/{subject}', 'SubjectsController@show');
    Route::put('subjects/{subject}', 'SubjectsController@update');
    Route::delete('subjects/{subject}', 'SubjectsController@delete');

    Route::post('tutorials', 'TutorialsController@store');
    Route::get('tutorials', 'TutorialsController@index');
    Route::get('tutorials/{tutorial}', 'TutorialsController@show');
    Route::put('tutorials/{tutorial}', 'TutorialsController@update');
    Route::delete('tutorials/{tutorial}', 'TutorialsController@delete');

    Route::post('quizes', 'QuizesController@store');
    Route::get('quizes', 'QuizesController@index');
    Route::get('quizes/{quiz}', 'QuizesController@show');
    Route::post('quizes/{quiz}/questions', 'QuizQuestionsController@store');
    Route::get('quizes/{quiz}/questions', 'QuizQuestionsController@index');
    Route::put('quizes/{quiz}', 'QuizesController@update');
    Route::delete('quizes/{quiz}', 'QuizesController@delete');

    Route::post('questions', 'QuestionsController@store');
    Route::get('questions', 'QuestionsController@index');
    Route::get('questions/{question}', 'QuestionsController@show');
});




