<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = \App\Course::all();
        $courses->each(function($course){
            $course->subjects()->saveMany(factory(\App\Subject::class, 3)->make());
        });
    }
}
