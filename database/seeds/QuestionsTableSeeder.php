<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quizes = \App\Quiz::all();
        $quizes->each(function($quiz){
            $quiz->questions()->saveMany(factory(\App\Question::class, 3)->make());
        });
    }
}
