<?php

use Illuminate\Database\Seeder;

class TutorialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = \App\Course::all();
        $courses->each(function($course){
            $course->subjects[0]->tutorials()->saveMany(factory(\App\Tutorial::class, 2)->make());
        });
    }
}
