<?php

use Illuminate\Database\Seeder;

class QuizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = \App\Course::all();
        $courses->each(function($course){
            $course->subjects[0]->quizes()->saveMany(factory(\App\Quiz::class, 2)->make());
        });
    }
}
