<?php

use Faker\Generator as Faker;

$factory->define(\App\Question::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence,
        'answer' => $faker->randomElement(['A', 'B', 'C', 'D'])
    ];
});
